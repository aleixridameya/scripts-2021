rep un més [1-12] més té 28,30,31
# existeix un arg
# arg és [1-12] 

#! /bin/bash
# @aleixridameya
# Febrer 2022
# Validar : ??? 
# ------------------------------------
# 1) si num args no es correcta plegar

if [ $# -ne 1 ]
then
   echo "Error: numero d'arguments incorrecte"
   echo "Usage: $0 mes"
   exit 1
fi

# 2) validar rang mes

if ! [ $1 -ge 1 -a $1 -le  12 ]
then
   echo "Error: el  numero de mes no és vàlid"
   echo "Usage: $0 mes"
   exit 2
fi

#xixa
case "$mes" in
  "2" )
    dies=28;;
"4"|"6"|"9"|"11")
    dies=30;;
*)
    dies=31;;
esac
echo "El mes $mes te $dies dies"
