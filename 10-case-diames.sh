#rep un més [1-12] més té 28,30,31
# existeix un arg
# arg és [1-12] 

#! /bin/bash
# @aleixridameya
# Febrer 2022
# Validar : ??? 
# ------------------------------------
# 1) si num args no es correcta plegar

if [ $# -ne 1 ]
then
   echo "Error: numero d'arguments incorrecte"
   echo "Usage: $0 mes"
   exit 1
fi

# 2) validar rang mes

if ! [ $1 -ge 1 -a $1 -le  12 ]
then
   echo "Error: el  numero de mes no és vàlid"
   echo "Usage: $0 mes"
   exit 2
fi

# 3) 
mes=$1
if [ $mes -eq 2 ]
then 
  echo "El mes $mes te 28 o 29 dies depenen l'any"
elif [ $mes -eq 4 -o $mes -eq 6 -o $mes -eq 9 -o $mes -eq 11 ] 
then
  echo "El mes $mes te 30 dies"
else
  echo "El mes $mes te 31 dies"
fi 
exit 0



