#! /bin/bash
# @aleixridameya
# Febrer 2022 
# Validar nota : suspés, aprovat 
# -------------------------------
ERR_NARGS=1
ERR_NOTA=2
# 1) si num args no es correcta plegar
if [ $# -ne 1 ]
then
   echo "Error: numero d'arguments incorrecte"
   echo "Usage: $0 nota"
   exit $ERR_NARGS
fi

# 2) validar rang nota
if ! [ $1 -ge 0 -a $1 -lt 10 ]
then
   echo "Error: numero d'arguments incorrecte"
   echo "nota pren valors de 0 a 10"
   echo "Usage: $0 nota"
   exit $ERR_NOTA
fi


# 3)
