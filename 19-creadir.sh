#! /bin/bash
# @aleixridameya
# Febrer 2022
# Validar : crear dir
# 0 tot dir creats ok
# 1 error en el numero d'arguments
# 2 si algun dir no s'ha pogut crear
# ------------------------------------
# 1) si num args no es correcta plegar
if [ $# -eq 0 ]
then
  exit 1
fi


for dir in $*
do

	mkdir $dir  2> /dev/null || error=2
	if [ $error -eq 2 ] ; then
	  echo "Error: en el directori $dir" 2> /dev/stderr
	  error=0
	  err_final=2
	fi
	
done
if [ $err_final -eq 2 ]
then
   exit 2
fi
exit 0

