
#! /bin/bash
# @aleixridameya
# Febrer 2022
# Descripcio: 
# ------------------------------
# 1) si num args no es correcte plegar
if [ $# -eq 0 ]
then
  echo "Error numero arguments incorrecte"
  echo "Usage: $0 dir"
  exit $ERR_NARGS
fi

args=$*
for opcio in $args
do

case "$opcio" in
	"-a"|"-b"|"-s"|"-n"|"-i")
       valid="$valid $opcio";;
 	 *)
       error="$error $opcio";;
esac
done
echo "Els arguments incorrectes són: $error"
echo "Arguments vàlids: $valid"
exit 0	
