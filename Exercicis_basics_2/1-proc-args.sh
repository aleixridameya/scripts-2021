#! /bin/bash
# @aleixridameya
# Febrer 2022
# Validar : Exercici 1 processar arguments
#----------------------------------------------------------------------
# 1) si num args no es correcta plegar

if [ $# -eq 0 ]
then
   echo "Error: numero d'arguments incorrecte"
   echo "Usage: $0 mes"
   exit 1
fi 
  
# Xixa
args=$*
for elem in $args
do
	chars=$( echo -n "$elem" | wc -c )
     
     if [ $chars -ge 4 ]
     then
     echo "$elem"
     fi
done
exit 0
