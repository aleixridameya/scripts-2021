#! /bin/bash
# @aleixridameya
# Febrer 2022
# Validar : Exercici 1 processar arguments
#----------------------------------------------------------------------
# 1) si num args no es correcta plegar

if [ $# -eq 0 ]
then
   echo "Error: numero d'arguments incorrecte"
   echo "Usage: $0 mes"
   exit 1
fi 
  
# Xixa
args=$*
compt=0
for elem in $args
do
	chars=$( echo -n "$elem" | wc -c )
     
     if [ $chars -ge 3 ]
     then
     ((compt++))
     fi
done
echo "Vegades $compt"
exit 0
