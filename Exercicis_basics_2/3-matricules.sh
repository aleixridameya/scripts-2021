#! /bin/bash
# @aleixridameya
# Febrer 2022
# Validar : Exercici 1 processar arguments
#----------------------------------------------------------------------
# 1) si num args no es correcta plegar

if [ $# -eq 0 ]
then
   echo "Error: numero d'arguments incorrecte"
   echo "Usage: $0 mes"
   exit 1
fi 

#2)
matricules=$*
comt=0
for matricula in $matricules
do 
    echo "$matricula" | grep -E "^[0-9]{4}-[A-Z]{3}$" 
    if [ $? -eq 0 ]
    then
	echo "Matricula $matricula correcte"

    else 
    ((comt++))
    echo "Error:la matricula $matricula no valida" 2> /dev/stderr
    fi

done
exit $comt 


