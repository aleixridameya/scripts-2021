#! /bin/bash
# @aleixridameya
# Febrer 2022
# Validar : Exercici 1 processar arguments
#----------------------------------------------------------------------
# 1) si num args no es correcta plegar

if [ $# -eq 0 ]
then
   echo "Error: numero d'arguments incorrecte"
   echo "Usage: $0 mes"
   exit 1
fi 


case "$1" in
	"-h"|"--help")

echo "Funcionament: prog file1 file2 file3..."
	
exit 0
esac


salida=0
file=$*
for elem in $file
do

	  

	if  ! [ -f $elem ]
	then
          echo "El file $elem no existeix" 2> /dev/stderr
  	else
	 gzip $elem 

		if [ $? -eq 0 ]
		then
		  echo "El file $elem s'ha comprimit correctament"
		  ((comprimit++))
		else
	  	  echo "El file $elem no s'ha poguit comprimir" 2> /dev/stderr
		  salida=2

	fi
	fi
done
echo "Comprimits: $comprimit"
exit $salida
