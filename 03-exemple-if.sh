
#! /bin/bash
# @aleixridamya AISX M01-ISO
# Feberer 2022
# Exemple processament d'arguments
#------------------------------------
# 1) validem argumentis

if [ $# -ne 1 ]
then
   echo "Error: numero d'arguments incorrecte"
   echo "Usage: $0 edat"
   exit 1

fi

#2 validar l'edat
edat=$1
if [ $edat -ge 18 ]
then
	echo "edat $edat és major d'edat"

else
	echo "edat $edat és menor d'edat"
fi
exit 0
