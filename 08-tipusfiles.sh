#! /bin/bash
# @aleixridameya
# Febrer 2022 
# Validar file : regular file , dir, link 
# -------------------------------
# 1) si num args no es correcta plegar
ERR_ARGS=1
ERR_NOEXIST=2
if [ $# -ne 1 ]
then
   echo "Error: numero d'arguments incorrecte"
   echo "Usage: $0 nota"
   exit $ERR_ARGS
fi


#Xixa
fit=$1
if [ ! -e $fit ]; then
   echo "Error: $fit no existeix"
   exit $ERR_NOEXIST
elif [ -h $fit ] ; then
	echo "$* és un link"
elif [ -d $fit ] ; then
	echo "$* és un direcotri"
elif [ -f $fit ] ; then
	echo "$fit és un regular file"
else 
	echo "$fit és una altre cosa"
fi
exit 0

