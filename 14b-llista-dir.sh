#! /bin/bash
# @aleixridameya
# Febrer 2022
# Descripcio:
# ------------------------------
ERR_NARGS=1
ERR_DIR=2
# 1) si num args no es correcte plegar
if [ $# -ne 1 ]
then
  echo "Error numero arguments incorrecte"
  echo "Usage: $0 dir"
  exit $ERR_NARGS
fi

#2)si no es un dir
if  ! [ -d $1  ]; then
  echo "Error: $dir no es un directori"
  echo "Usage: $0 directori"
  exit $ERR_DIR
fi
#3)xixa
dir=$1

num=1
llista_ls=$(ls $dir)

for list in $llista_ls 
do
  echo "$num : $list" 
  ((num++))
done
exit 0

	

