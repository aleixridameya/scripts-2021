#! /bin/bash
# @aleixridameya
# Febrer 2022 
# Validar file : exemple while
# -------------------------------

while [ -n "$1" ]
do
  echo "$1"
  shift
done



#mostrar un comptador del 1 a MAX

comptador=$1
while [ $comptador -ge 0 ]
do
	echo "$comptador"
	((comptador--))
done
exit 0

#Mostrar un comptador del 1 a max
MAX=10
comptador=0
while [ $comptador -le $MAX ]
do
  echo "$comptador"
  ((comptador++))
done
exit 0
