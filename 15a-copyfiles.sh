#! /bin/bash
# @aleixridameya
# Febrer 2022
# Descripcio: 
#------------------------------------------------
# 1) si num args no es correcte plegar
if [ $# -ne 2 ]
then
  echo "Error numero arguments incorrecte"
  echo "Usage: $0 dir"
  exit 1
fi
# 2) validar args
if ! [ -f $1 ] 
then
 echo "Error l'argument $1 no és un regular file"
 echo "Usage: $0 file"
 exit 2

elif ! [ -d $2 ]
then
 echo "Error l'argument $2 no és un directori"
 echo "Usage: $0 file"
 exit 2
fi

# xixa
file=$1
dir=$2
cp $file $dir
