#! /bin/bash
# @aleixridameya
# Febrer 2022
# Descripcio: 
#------------------------------------------------
# 1) si num args no es correcte plegar
if [ $# -lt 2 ]
then
  echo "Error numero arguments incorrecte"
  echo "Usage: $0 dir"
  exit 1
fi

desti=$(echo $* | cut -d' ' -f($#))
echo "desti : $desti"

llista_files=$(echo $* | sed 's/ [^ ]*$//g')

# 3) validar desti
if ! [ -d $desti ]
then
 echo "Error l'argument $desti no és un directori"
 echo "Usage: $0 file"
 exit 2
fi

# 4) iterar file a file
for files in $llista_files
do
  if ! [ -f $files ] 
  then
    echo "Error l'argument $files no és un regular file"
  else
    cp $files $desti
fi
done
exit 0
  
