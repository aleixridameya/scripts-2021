#! /bin/bash
# @aleixridameya
# Febrer 2022 
# Validar nota : suspés, aprovat, notable, excel·lent 
# -------------------------------
# 1) si num args no es correcta plegar
ERR_ARGS=1
ERR_NOTA=2
if [ $# -ne 1 ]
then
   echo "Error: numero d'arguments incorrecte"
   echo "Usage: $0 nota"
   exit $ERR_ARGS
fi
# 2) validar rang nota
nota=$1
if ! [ $nota -ge 0 -a $nota -le 10 ]
  then
   echo "Error: numero d'arguments incorrecte"
   echo "Usage: $0 nota"
   exit $ERR_NOTA
fi
#xixa
nota=$1
if [ $nota -lt 5 ] ; then
	echo "La nota $nota és un suspés"
elif [ $nota -lt 7 ]; then
	echo "La nota $nota és un aprovat"
elif [ $nota -lt 9 ] ; then
	echo "La nota $nota és un notable"
else
	echo "La nota $nota és un excel·lent"
fi
exit 0 
