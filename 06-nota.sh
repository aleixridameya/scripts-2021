#! /bin/bash
# @aleixridameya
# Febrer 2022 
# Validar nota : suspès, aprovat, notable, excelent.
# -------------------------------
# 1) si num args no es correcta plegar

if [ $# -ne 1 ]
then
   echo "Error: numero d'arguments incorrecte"
   echo "Usage: $0 nota"
   exit 1
fi
# 2) validar rang nota
nota=$1
if ! [ $nota -ge 0 -a $nota -le 10 ]
  then
   echo "Error: numero d'arguments incorrecte"
   echo "Usage: $0 nota"
   exit 1
fi
# 3) Xixa
nota=$1
if [ $nota -lt 5 ]
then 
  echo "la nota $nota és suspés"

elif [ $nota -eq 6 -o $nota -eq 5 ]
then 
  echo "la nota $nota és aprovat"

elif [ $nota -eq 7 -o $nota -eq 8 ]
then 
  echo "la nota $nota és notable"

else 
  echo "la nota $nota és excel·lent"

fi
exit 0
