#! /bin/bash
# @aleixridameya
# Febrer 2022
# Validar : Processar línia a línia l’entrada estàndard, si la línia té més de 60 caràcters lamostra, si no no.
#----------------------------------------------------------------------
# 1) si num args no es correcta plegar

if [ $# -eq 0 ]
then
   echo "Error: numero d'arguments incorrecte"
   echo "Usage: $0 mes"
   exit 1
fi 
  
#2)
noms=$*
for nom in $noms
do
	grep -q  "^$nom:" /etc/passwd 
 
  if [ $? -eq 0 ]
    then
    echo "nom $nom vàlid"
 
  else  
    echo "Error: Nom $nom no vàlid" 2> /dev/stderr 
    
  fi
done
exit 0
