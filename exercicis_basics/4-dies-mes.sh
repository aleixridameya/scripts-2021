#! /bin/bash
# @aleixridameya
# Febrer 2022
# Validar : dies de mes segons el numero del mes 
# ------------------------------------
# 1) si num args no es correcta plegar

if [ $# -lt 1 ]
then
   echo "Error: numero d'arguments incorrecte"
   echo "Usage: $0 mes"
   exit 1
fi

# 2)validar del 1 al 12
mesos_anys=$*
for mes in $mesos_anys
do
if ! [ $mes -ge 1 -a $mes -le  12 ]
then
   echo "Error: el numero de mes $mes no és vàlid" 2> /dev/stderr 
# 3) Xixa
else
case $mes in
	"2")
	dies_mes=28;;
  "4"|"6"|"9"|"11")
	dies_mes=30;;
         *)
        dies_mes=31;;
esac
echo "el mes $mes te $dies_mes dies"
fi
done
exit 0
