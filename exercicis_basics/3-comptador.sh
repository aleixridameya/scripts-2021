#! /bin/bash
# @aleixridameya
# Febrer 2022
# Validar : comptador de 0 fins el valor indicat
# ------------------------------------
# 1) si num args no es correcta plegar

if [ $# -ne 1 ]
then
   echo "Error: numero d'arguments incorrecte"
   echo "Usage: $0 mes"
   exit 1
fi

# 2) xixa
max=$1
num=0
while [ $num -le $max ]
do
  echo " $num "
  ((num++))
done
exit 0
