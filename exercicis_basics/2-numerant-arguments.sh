#! /bin/bash
# @aleixridameya
# Febrer 2022
# Mostrar l’entrada estàndard numerant línia a línia
# -------------------------------
# 1) Validar numero d'arguments
if [ $# -lt 1 ]
then
   echo "Error: numero d'arguments incorrecte"
   echo "Usage: $0 argument"
   exit 1
fi
# Xixa 
arguments=$*
num=1
for elem in $arguments
do
  echo "$num: $elem"
  ((num++))
done
exit 0


