#! /bin/bash
# @aleixridameya
# Febrer 2022
# Validar : Mostrar línia a línia l’entrada estàndard, retallant només els primers 50 caràcters
#----------------------------------------------------------------------

#xixa
while read -r line
do
	echo "$line" | cut -c1-50
done 
exit 0
