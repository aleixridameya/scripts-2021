#! /bin/bash
# @aleixridameya
# Febrer 2022
# Validar : Processar línia a línia l’entrada estàndard, si la línia té més de 60 caràcters lamostra, si no no.
#----------------------------------------------------------------------
# 1) si num args no es correcta plegar
  
#2)
noms=$*

while read -r nom
do
  grep -q  "^$nom:" /etc/passwd 
 
  if [ $? -eq 0 ]
    then
    echo "nom $nom vàlid"
 
  else  
    echo "Error: Nom $nom no vàlid" 2> /dev/stderr 
    
  fi
done
exit 0

