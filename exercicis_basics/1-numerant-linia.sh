#! /bin/bash
# @aleixridameya
# Febrer 2022
# Mostrar l’entrada estàndard numerant línia a línia
# -------------------------------
# 1) Xixa
num=1
while read -r line
do
  echo "$num: $line"
  ((num++))
done
exit 0

