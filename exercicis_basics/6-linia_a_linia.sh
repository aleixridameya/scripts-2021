#! /bin/bash
# @aleixridameya
# Febrer 2022
# Validar : Processar línia a línia l’entrada estàndard, si la línia té més de 60 caràcters lamostra, si no no.
#----------------------------------------------------------------------
#Xixa
while read -r line
do
	num_paraula=$(echo "$line" | wc -c)
	if [ $num_paraula -ge 60 ]
	then 
	echo "$line"
fi
done
