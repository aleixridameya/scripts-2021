#! /bin/bash
# @aleixridameya
# Febrer 2022
# Validar :
#----------------------------------------------------------------------
# 1) si num args no es correcta plegar

if ! [ $# -eq 1 ]
then
   echo "Error: numero d'arguments incorrecte"
   echo "Usage: $0 mes"
   exit 1
fi 

#2) xixa
MAX=$1
compt=1
while read -r line
do
  echo "$compt: $line"
  
   if [ $compt -eq $MAX ]
   then
   exit 0
fi
((compt++))
done
