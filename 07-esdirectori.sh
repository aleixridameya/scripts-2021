#! /bin/bash
# @aleixridameya
# Febrer 2022 
# -------------------------------
# 1) si num args no es correcta plegar
ERR_ARGS=1
ERR_NODIR=2
if [ $# -ne 1 ]
  then
   echo "Error: numero d'arguments incorrecte"
   echo "Usage: $0 dir"
   exit $ERR_ARGS
fi
# 2) validar si és un dir 
if [ ! -d $1  ]
  then
   echo "Error: $1 No és un direcori"
   echo "Usage: $0 dir"
   exit $ERR_NODIR
fi
#Xixa
dir=$1
ls $dir
exit 0 
