#! /bin/bash
# @aleixridameya
# ASIX M01-ISO
# Exemples funcions

function hola(){
	echo "hola"
	return 0
}

function dia(){
	date
	return 0
}

function suma(){
	echo $(($1+$2))
	return 0
}

function all(){
	hola
	echo "avui som: $(dia)"
	suma 6 9
}

function fsize(){
	#)1 Validar existeix loginç
	login=$1
	linia=$(grep "^$login:" /etc/passwd)
	if [ -z "$linia" ]; then 
	  echo "Error: user $login inexistent"
	  return 1
	 fi
	 
	#)2 Obtenir el home dir 
        dirhome=$(echo $linia | cut -d: -f6)


	#)3 Calcular du -sh
	du -sh $dirhome
	return 0
}

#rep logins i per a cada login es mostra l'ocupació de disc del home de l'usuari usant fsizw

function loginargs(){ 
	#1) Validar almenys es reo 1 login
	if [ $# -eq 1 ] ; then
	  echo "Error: numero d'arguments $# invàlid"
  	  return 1
	fi	  
	#2) iterar per cada login
	for login in $*
	do 
	    fsize $login
	done
}
# 3) loginfile
# Rep com a argument un nom de fitxer que conté un login per línia.
# Mostrar l'ocupació de disc de cada usuari usant fsize.
# Verificar que es rep un argument i que és un regular file.
  # Validar existeix fitxer

  function loginfile(){

  if [ ! -f "$1" ] ; then
	 echo " fitxer  inexistent"
	return 1
   fi
   filein=$1
   while read -r login
   do
	fsize $login
   done < $filein	
}


# 4) loginboth [file] 
# processa file p stdin, mostra fsize dels usersç

function loginboth(){
	filein=/dev/stdin
	if [ $# -eq 1 ] ; then
		filein=$1
	fi
	while read -r login
	do
		fsize $login
	done < $filein

}

# 5 grepid 
#Validar que es un arg
function grepid(){
	if [ $# -ne 1 ]
	then
	   echo "Error num arg invàlid"
	   return 1
	fi
#validar que es un gid vàlid
    gid=$1
    grep "^[^:]*:[^:]*:$gid:" /etc/group
    if [ $? -ne 0 ] 
    then
	    echo "Error gid $gid no vàlid"
	    return 2
    fi
# retornar la llista de login que tenen aquest grup com a grup principal
   logs_gid=$(cut -d: -f1,4 /etc/passwd | grep ":$gid$" | cut -d: -f1)
   echo "$logs_gid"
}

#6 Donat 1 gid com a arg,  mostrar per a cada user que pertany
# grup l'ocupació  de disc del sue home
function gidsize(){
gid=$1
login_list=$(grepid $gid )
 for login in $login_list
 do
     fsize $login
 done

}

# 7

function allgidsize(){
	llista_gid=$( cut -d: -f4 /etc/passwd | sort -gu)	
	for gid in $llista_gid
	do 
	 echo "GID: $gid"
	 gidsize $gid
	done

}


# 8) filtergroup
# llistar les linies del /etc/passwd dels users dels groups del 0 al 100
function filtergroup(){
	file=$1
	while read -r line
	do
		gid=$(echo  $line | cut -d: -f4)
		if [ $gid -ge 0 -a $gid -le 100 ] ; then
		 echo $line
		fi
	done < $file
}

